package slide;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import sendmepro.cl.nexo.R;

public class Sample extends PagerAdapter {
    LayoutInflater metela;
    Context cont;
    private static int[] ls_img = {
            R.drawable._x0,
            R.drawable._x1,
            R.drawable._x2
    };

    public void setNewDrawableSamples(int actor[]){
        ls_img = actor;
    }

    public Sample(Context puta){
        this.cont = puta;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        metela = (LayoutInflater)cont.getSystemService(cont.LAYOUT_INFLATER_SERVICE);
        View v = metela.inflate(R.layout.frame,container,false);
        ConstraintLayout ohh = (ConstraintLayout) v.findViewById(R.id.layout_slide);
        ImageView uhh = (ImageView)v.findViewById(R.id.img_slider);
        uhh.setImageResource(ls_img[position]);
        container.addView(v);
        return v;
        //return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout)object);
        //super.destroyItem(container, position, object);
    }

    @Override
    public int getCount(){
        return ls_img.length;
    }
    @Override
    public boolean isViewFromObject(View v,Object o){
        return v==(ConstraintLayout)o;
    }
}
