package static_tmp;

public class ImageView {
    private String title;
    private int imageUrl;

    public ImageView(String title,int imageUrl){
        this.title = title;
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public int getImageUrl() {
        return imageUrl;
    }
}
