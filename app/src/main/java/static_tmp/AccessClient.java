package static_tmp;

import android.graphics.drawable.Drawable;
import android.os.StrictMode;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class AccessClient {
    private static int usuario;
    private static String token_sync;
    private static String nombre_completo;
    private static String cargos;
    private static DateTime acceso;
    private static Drawable avatarStable;
    public AccessClient(){

    }
    public static Drawable createDrawableFromURL(String urlString) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Drawable image = null;
        try {
            URL url = new URL(urlString);
            InputStream is = (InputStream)url.getContent();
            image = Drawable.createFromStream(is, "src");
        } catch (MalformedURLException e) {
            // handle URL exception
            image = null;
        } catch (IOException e) {
            // handle InputStream exception
            image = null;
        }
        return image;
    }

    public boolean openManageSync(){
        return !cargos.contains("Apoderado");
    }
    public String[] getSimpleData(){
        try {
            String[] comeme = {nombre_completo, cargos, token_sync, acceso.toLocalDate().toString()};
            return comeme;
        }catch(NullPointerException p) {
            return null;
        }
    }
    public Drawable getAvatarStable(){
        return avatarStable;
    }
    public static void rewriteTmpAccess(JSONObject access){
        try{
            usuario = Integer.parseInt(access.getString("run"));
            nombre_completo = access.getString("nombre_completo");
            cargos = access.getString("cargo");
            token_sync = access.getString("token");
            avatarStable = createDrawableFromURL("https://nexoapp.cl/api/?openImg=" + access.getString("image"));
            /*SimpleDateFormat manualParse=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            acceso = manualParse.parse(access.getString("ultimo_acceso"));*/
            acceso = new DateTime();
        }catch(JSONException e){

        }
    }
    public void accessReset(){
        usuario = 0;
        acceso = null;
        token_sync = null;
        nombre_completo = null;
        cargos = null;
        avatarStable = null;
        client.Jdata.jsonRemoval();
    }
}
