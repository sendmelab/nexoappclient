package require;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.math.BigDecimal;
import java.math.RoundingMode;

import sendmepro.cl.nexo.R;
import static_tmp.AccessClient;

public class StaticString {
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    public static AlertDialog pantallaDeBienvenida (Context hoa){
        AccessClient cli = new AccessClient();
        AlertDialog.Builder internal = new AlertDialog.Builder(hoa);
        internal.setTitle(R.string.hello);
        internal.setMessage("Bienvenido/a "+cli.getSimpleData()[0]+"\nSomos \"Nexo App comunidad\"\n\n y te damos la bienvenida.");
        return internal.create();
    }
    public static AlertDialog getInternalNotaDialog(Context grueso, int[] labels,String mes){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(grueso);
        alertDialogBuilder.setTitle(labels[0]);
        alertDialogBuilder.setIcon(labels[1]);
        alertDialogBuilder.setMessage(mes);
        alertDialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return alertDialogBuilder.create();
    }
    public int getMonthlyStaticString(byte monthDate){
        int staticString = R.string.jan;
        switch(monthDate){
            case 2:
                staticString = R.string.fer;
                break;
            case 3:
                staticString = R.string.mar;
                break;
            case 4:
                staticString = R.string.apr;
                break;
            case 5:
                staticString = R.string.may;
                break;
            case 6:
                staticString = R.string.jun;
                break;
            case 7:
                staticString = R.string.jul;
                break;
            case 8:
                staticString = R.string.aug;
                break;
            case 9:
                staticString = R.string.sep;
                break;
            case 10:
                staticString = R.string.oct;
                break;
            case 11:
                staticString = R.string.nov;
                break;
            case 12:
                staticString = R.string.dec;
                break;
        }
        return staticString;
    }
}
