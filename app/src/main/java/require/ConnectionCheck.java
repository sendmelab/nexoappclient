package require;

import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;

public class ConnectionCheck {
    public static boolean checkIfURLExists(String targetUrl) {
        HttpURLConnection httpUrlConn;
        try {
            httpUrlConn = (HttpURLConnection) new URL(targetUrl)
                    .openConnection();
            httpUrlConn.setRequestMethod("HEAD");
            httpUrlConn.setConnectTimeout(10000);
            httpUrlConn.setReadTimeout(10000);
            //System.out.println("Response Code: " + httpUrlConn.getResponseCode());
            //System.out.println("Response Message: " + httpUrlConn.getResponseMessage());
            Log.d("STATUS",String.valueOf(httpUrlConn.getResponseCode()));
            return (httpUrlConn.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            //System.out.println("Error: " + e.getMessage());
            return false;
        }
    }
}
