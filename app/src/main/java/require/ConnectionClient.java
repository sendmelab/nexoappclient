package require;

import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import client.ClientSession;
import sendmepro.cl.nexo.Login;
import static_tmp.AccessClient;

public class ConnectionClient {
    private Cache cache;
    private BasicNetwork internet;
    private RequestQueue respuesta;
    private JSONObject object_response;
    private Map<String, String> internalParams = new HashMap<String, String>();
    private String internalRoute = null;
    private StringRequest requestTransferData;
    public ConnectionClient(File cacheDir){
        cache = new DiskBasedCache(cacheDir,1024*1024);
        internet = new BasicNetwork(new HurlStack());
        respuesta = new RequestQueue(cache,internet);
    }

    private StringRequest updateRequestAddr(){
        return new StringRequest(Request.Method.POST, "https://nexoapp.cl/api"+(internalRoute!=null ? "/index.php/"+internalRoute:"/"),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        client.Jdata static_data = new client.Jdata();
                        try{
                            JSONObject mode = new JSONObject(response);
                            if(internalRoute!=null){
                                switch(internalRoute){
                                    case "init":
                                        Login.responseRequest(mode,false);
                                        break;
                                }
                            }
                            Log.d("DEBUGGER JSON",mode.toString());
                            if(mode.has("gets")){
                                static_data.downloadTempStableData(new JSONObject(mode.getString("gets")));
                            }else if(mode.has("request")){
                                static_data.polliyaEnabled(Byte.valueOf(mode.getString("request")));
                            }
                        }catch(JSONException | NullPointerException eputa){
                            Log.e("Remote Failure",eputa.getMessage());
                            static_data.setErrorAndStopEternalLoop();
                        }
                        respuesta.stop();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        try {
                            Log.e("Error", error.getMessage());
                            if(internalRoute!=null){
                                switch(internalRoute){
                                    case "init":
                                        Login.responseRequest(null,true);
                                        break;
                                }
                            }
                        }catch(NullPointerException sp){

                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                return internalParams;
            }
        };
    }
    private void updateTransactionMethod(){
        requestTransferData = updateRequestAddr();
        respuesta.add(requestTransferData);
        respuesta.start();
    }
    private void loadSavedSessionState(){
        AccessClient phone = new AccessClient();
        internalParams.put("id",phone.getSimpleData()[0]);
        internalParams.put("token",phone.getSimpleData()[2]);
    }
    public void updateRetrieveListener (String b,String[] mine){
        loadSavedSessionState();
        internalParams.put("qua",b);
        internalParams.put("label",String.valueOf(mine.length));
        internalRoute = "setup";
        for(byte oh=0;oh<mine.length;oh++){
            internalParams.put("sender"+oh,mine[oh]);
        }
        updateTransactionMethod();
    }
    public void actionRetrieveListener(byte load,String con,String secondCon){
        loadSavedSessionState();
        internalParams.put("listRequest",String.valueOf(load));
        if (con!=null){
            internalParams.put("alumnoName",con.trim());
        }
        if(secondCon!=null){
            internalParams.put("secondAlumnoName",secondCon.trim());
        }
        internalRoute = null;
        updateTransactionMethod();
    }
    public void actionRemoteLoginServer(ClientSession client){
        internalParams.put("user_var",client.getTmpUserType(false));
        internalParams.put("psw_var",client.getTmpUserType(true));
        internalRoute = "init";
        updateTransactionMethod();
    }
    public void actionRegisterConfirmation(JSONObject sampleRegister,String route){
        object_response = sampleRegister;
        String modal = (route!=null ? "index.php/"+route:null);
        StringRequest strRequest = new StringRequest(Request.Method.POST, "https://nexoapp.cl/api/",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        try{
                            JSONObject mode = new JSONObject(response);
                            Log.d("RESPUESTA", mode.toString());
                        }catch(JSONException eputa){
                            Log.e("Error Puto",eputa.getMessage());
                        }
                        respuesta.stop();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        try{
                            Log.e("Error",error.getMessage());
                        }catch(NullPointerException spe){

                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                try{
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("nexoSenderClient", object_response.getString("personal_email"));
                    params.put("nexoSenderType", object_response.getString("personal_charge"));
                    return params;
                }catch(JSONException exp){
                    return null;
                }
            }
        };
        respuesta.add(strRequest);
        respuesta.start();
    }

    public void actionRegisterCustomConfirmation(Map<String, JSONObject> command){
        Log.d("CHATO",command.toString());
        JsonObjectRequest jsArrayRequest = new JsonObjectRequest(
                Request.Method.POST,
                "https://amadeusweb.com/nexo_tmp/",
                new JSONObject(command),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Manejo de la respuesta
                        Log.d("DATA",response.toString());
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Manejo de errores
                        Log.e("Error",error.getMessage());
                    }
                });
        respuesta.add(jsArrayRequest);
        respuesta.start();
        /*
            String  tag_string_req = "nexoSenderClient";
            Map<String, String> params = new HashMap<String, String>();
            params.put("nexoSenderClient","CMD0005");

            JSONObject jsonObj = new JSONObject(params);
            String url="https://amadeusweb.com/nexo_tmp/"; //your link
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObj, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("responce", response.toString());

                    try {
                        // Parsing json object response
                        // response will be a json object
                        String userbalance = response.getString("userbalance");
                        Log.d("userbalance",userbalance);
                        String walletbalance = response.getString("walletbalance");
                        Log.d("walletbalance",walletbalance);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("ErrorJSON",e.getMessage());
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VolleyError",error.getMessage());
                }
            });
*/
            //AppControllerVolley.getInstance().addToRequestQueue(jsonObjReq, tag_string_req);
    }

    public void actionRegisterTestConfirmation(JSONObject comms){
        try{
            JSONObject resend = new JSONObject();
            resend.put("nexoSenderClient",comms);
            Log.d("SENDING",resend.toString());

            respuesta.start();
            JsonObjectRequest objetivo = new JsonObjectRequest(Request.Method.POST, "https://amadeusweb.com/nexo_tmp/", resend,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.i("REQUEST",response.toString());
                            respuesta.stop();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            /*
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, "https://amadeusweb.com/nexo_tmp/", resend, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("CONSOLE",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR VOLLEY",error.getMessage());
                }
            });*/
            respuesta.add(objetivo);
        }catch(JSONException exp){
            Log.e("Read error",exp.getMessage());
        }
    }
}
