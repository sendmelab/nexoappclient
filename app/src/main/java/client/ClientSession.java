package client;

import crypto.Encrypt;

public class ClientSession {
    private String user_name;
    private String user_psw;
    public ClientSession(){

    }
    public void setCredentialAuthMode(String usw,String psw,boolean isChanger){
        user_name = usw;
        user_psw = Encrypt.generateMD5(psw);
    }
    public String getTmpUserType(boolean section){
        return section ? user_psw : user_name;
    }
}
