package sendmepro.cl.nexo;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import slide.Sample;

public class Demo extends AppCompatActivity {
    private ViewPager muestra;
    private Sample s;
    @Override
    protected void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.nexo_sample);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        muestra = (ViewPager)findViewById(R.id.demo_sample);
        s = new Sample(this);
        int[] foll = {R.drawable.ojo2,R.drawable.ojo5,R.drawable.ojo0,R.drawable.ojo3,R.drawable.ojo4,R.drawable.ojo1,R.drawable.ojo6};
        s.setNewDrawableSamples(foll);
        muestra.setAdapter(s);
    }
}
