package sendmepro.cl.nexo.ref_main;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import client.Jdata;
import de.hdodenhof.circleimageview.CircleImageView;
import require.ConnectionClient;
import require.StaticString;
import sendmepro.cl.nexo.R;
import static_tmp.AccessClient;
import static_tmp.DatePickerFragment;

public class Main extends AppCompatActivity {
    private Handler time = new Handler();
    private Boolean lockActivity = false;
    private Drawable[] downloadedImage;
    private String[] selectedOptions;
    private TextView getNombre,miCargo,mimenu,micurso,miRightInfo;
    private ImageButton goBackHere,calendarSelector;
    private Button goHome;
    private AccessClient agrio = new AccessClient();
    private Jdata stable = new Jdata();
    private CircleImageView avatarImg;
    private byte current_screen = 0;
    private LinearLayout another,second_menu;
    private LocalDate selected_custom_date = new LocalDate();
    private JSONObject getting_data = new JSONObject();

    @Override
    protected void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        another = (LinearLayout)findViewById(R.id.anotherLayout);
        second_menu = (LinearLayout) findViewById(R.id.secondframe);
        goHome = (Button)findViewById(R.id.homes);
        calendarSelector = (ImageButton)findViewById(R.id.imzCalendar);
        goBackHere = (ImageButton)findViewById(R.id.imHome);
        mimenu = (TextView)findViewById(R.id.txtMenus);
        getNombre = (TextView)findViewById(R.id.txtFullName);
        micurso = (TextView)findViewById(R.id.gradeInfo);
        miRightInfo = (TextView)findViewById(R.id.leftTxtInfo);
        miCargo = (TextView)findViewById(R.id.txtCargo);

        avatarImg = (CircleImageView)findViewById(R.id.avatarImg);

        onPerfilMenu();
        goBackHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        goHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        calendarSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                DatePickerFragment fragment = new DatePickerFragment();
                fragment.setup(calendar, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        DateTimeFormatter formato = DateTimeFormat.forPattern("dd-MM-yyyy");
                        selected_custom_date = formato.parseLocalDate(dayOfMonth+"-"+month+"-"+year);
                        try{
                            TextView viewDate = (TextView)findViewById(R.id.fechaSelect);
                            viewDate.setText("Fecha: "+selected_custom_date.toString());
                        }catch(NullPointerException ne){

                        }
                    }
                });
                fragment.show(getSupportFragmentManager(), null);
            }
        });
        StaticString.pantallaDeBienvenida(Main.this).show();
        avatarImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_screen == 0){
                    current_screen = 100;
                    onPerfilMenu();
                }
            }
        });
    }
    public static int getFirstDate(int year, int month, int weeks) {
        Calendar cache = Calendar.getInstance();
        cache.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cache.set(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
        cache.set(Calendar.MONTH, month);
        cache.set(Calendar.YEAR, year);
        return cache.get(Calendar.DATE);
    }
    private void strongValidatorCheck(String[] semola,String[] devo){
        try{
            for(byte found=0;found<semola.length;found++){
                JSONArray mamado = new JSONArray(stable.getRemoteStableData().getString(semola[found]));
                for(int i=0;i<mamado.length();i++){
                    JSONObject obj = new JSONObject(mamado.getString(i));
                    if (obj.getString("RAMO").equalsIgnoreCase(devo[0])){
                        Log.d("VALID","YES");
                    }
                }
            }
        }catch(NullPointerException | JSONException none){

        }
    }
    @Override
    public void onBackPressed(){
        if (!lockActivity) {
            if (current_screen == 0) {
                agrio.accessReset();
                super.onBackPressed();
            } else if (current_screen == 4) {
                current_screen = 3;
                onPerfilMenu();
            } else if (current_screen == 1){
                onShowingMainAccessController(true);
                current_screen = 5;
                onPerfilMenu();
            } else{
                current_screen = 0;
                onPerfilMenu();
            }
        }
    }

    private void onShowingMainAccessController(boolean inShow){
        goHome.setVisibility(inShow ? View.VISIBLE:View.GONE);
        avatarImg.setVisibility(inShow ? View.VISIBLE:View.GONE);
        getNombre.setVisibility(inShow ? View.VISIBLE:View.GONE);
        miCargo.setVisibility(inShow ? View.VISIBLE:View.GONE);
        calendarSelector.setVisibility(inShow ? View.GONE:View.VISIBLE);
    }
    private void onShowmeYoungProfile(boolean inShowNow){
        micurso.setVisibility(inShowNow ? View.VISIBLE:View.GONE);
    }
    private void gettingScreenShow(byte leche){
        micurso.setText(stable.getInteropTempValue(Byte.valueOf("1"))[leche]);
        avatarImg.setImageDrawable(downloadedImage[leche]);
        getNombre.setText(stable.getInteropTempValue(Byte.valueOf("0"))[leche]);
        miCargo.setVisibility(View.GONE);
    }
    private void onPerfilMenu(){
        ConnectionClient client = new ConnectionClient(getCacheDir());
        switch(current_screen){
            default:
                stable.interactSetCompiler(null);
                miRightInfo.setVisibility(View.GONE);
                second_menu.removeAllViews();
                avatarImg.setImageDrawable(agrio.getAvatarStable());
                getNombre.setText(agrio.getSimpleData()[0]);
                miCargo.setText(agrio.getSimpleData()[1]);
                mimenu.setText(agrio.openManageSync() ? "Perfil "+agrio.getSimpleData()[1]:"Perfil Apoderado");
                another.removeAllViews();
                another.addView(LayoutInflater.from(this).inflate(agrio.openManageSync() ? R.layout.profesor_menu:R.layout.person_apoderado, another, false));

                TextView date = (TextView)findViewById(R.id.txLastAccess);
                addingToolbarFrame(false);
                date.setText("Ultimo Acceso:\n"+agrio.getSimpleData()[3]);
                Jdata.jsonRemoval();
                if(agrio.openManageSync()){
                    TextView envio_total = (TextView)findViewById(R.id.butter_num);
                    Button evaluacion = (Button)findViewById(R.id.btnEvaluate);
                    evaluacion.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            current_screen = 5;
                            onPerfilMenu();
                        }
                    });
                    client.actionRetrieveListener(Byte.valueOf("11"),null,null);
                    lockActivity = true;
                    time.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!stable.gettingErrorCallback()){
                                JSONObject sos = stable.getRemoteStableData();
                                if (sos!=null){
                                    lockActivity = false;
                                    try{
                                        TextView tx = (TextView)findViewById(R.id.butter_num);
                                        tx.setText(sos.getString("enviados"));
                                    }catch(JSONException e){

                                    }
                                }else{
                                    time.postDelayed(this,1000);
                                }
                            }else{
                                lockActivity = false;
                            }
                        }
                    },1000);
                }else{
                    onShowmeYoungProfile(false);
                    ConstraintLayout evaluate = (ConstraintLayout)findViewById(R.id.evalSelect);
                    client.actionRetrieveListener(Byte.valueOf("5"),null,null);
                    lockActivity = true;
                    time.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!stable.gettingErrorCallback()) {
                                JSONObject ajo = stable.getRemoteStableData();
                                if (ajo != null) {
                                    lockActivity = false;
                                    time.removeCallbacks(this);
                                    try {
                                        TextView evaCount = (TextView) findViewById(R.id.counterEva);
                                        evaCount.setText(ajo.getString("sin_revision"));
                                        ConstraintLayout feo = (ConstraintLayout)findViewById(R.id.evalSelect);
                                        feo.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                current_screen = 2;
                                                onPerfilMenu();
                                            }
                                        });
                                    } catch (JSONException e) {

                                    }
                                } else {
                                    time.postDelayed(this, 500);
                                }
                            }else{
                                time.removeCallbacks(this);
                                lockActivity = false;
                            }
                        }
                    }, 300);
                }
                onShowingMainAccessController(true);
                break;
            case 1:
                Jdata.jsonRemoval();
                miRightInfo.setVisibility(View.GONE);
                second_menu.removeAllViews();
                //selected_custom_date = new LocalDate();
                mimenu.setText(R.string.evaluate);
                getting_data = new JSONObject();
                selectedOptions = new String[1];

                another.removeAllViews();
                another.addView(LayoutInflater.from(this).inflate(R.layout.evaluator_menu, another, false));
                Spinner grade = (Spinner)findViewById(R.id.settingGrade);
                String[] culto = {"Curso","1º basico","2º basico","3º basico","4º basico","5º basico","6º basico","7º basico","8º basico","1º medio","2º medio","3º medio","4º medio"};
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, culto);
                grade.setAdapter(adapter);
                AutoCompleteTextView ramo = (AutoCompleteTextView) findViewById(R.id.settingRamo);
                ramo.setThreshold(1);
                AutoCompleteTextView asunto = (AutoCompleteTextView)findViewById(R.id.settingAsunto);
                asunto.setThreshold(1);
                if (selected_custom_date!=null){
                    TextView tm = (TextView)findViewById(R.id.fechaSelect);
                    tm.setText("Fecha: "+selected_custom_date.toString());
                }

                grade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if(position==0){
                            Toast.makeText(getApplication(),"Por favor debe seleccionar el curso correspondiente.",Toast.LENGTH_LONG).show();
                        }else{
                            Jdata.jsonRemoval();
                            selectedOptions = new String[1];
                            ConnectionClient clic = new ConnectionClient(getCacheDir());
                            clic.actionRetrieveListener(Byte.valueOf("10"),String.valueOf(position),null);

                            LinearLayout s_temp = (LinearLayout)findViewById(R.id.tableAlumnoItems);
                            s_temp.removeAllViews();
                            TextView sel = (TextView)findViewById(R.id.capturedAlumnoSelected);
                            sel.setText(null);
                            //TableLayout cagao = (TableLayout)findViewById(R.id.tableAlumnoItems);
                            //cagao.removeAllViews();
                            //cagao.setVisibility(View.INVISIBLE);
                            time.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    JSONObject obj = stable.getRemoteStableData();
                                    if (obj!=null){
                                        try{
                                            JSONArray are = new JSONArray(obj.getString("range"));
                                            //TableLayout temp = (TableLayout) findViewById(R.id.tableAlumnoItems);
                                            //TableRow tr = new TableRow(getApplication());
                                            //tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                                            LinearLayout temp = (LinearLayout)findViewById(R.id.tableAlumnoItems);

                                            String[][] seca = new String[1][are.length()];
                                            for(int m=0;m<are.length();m++){
                                                JSONObject z = new JSONObject(are.getString(m));
                                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                                final View rowme = inflater.inflate(R.layout.inflater_tableprof, null);
                                                temp.addView(rowme, temp.getChildCount()-1);
                                                CircleImageView remoteImg = (CircleImageView)findViewById(R.id.ava);
                                                remoteImg.setImageDrawable(agrio.createDrawableFromURL("https://nexoapp.cl/api/?openImg="+z.getString("avatar")));
                                                remoteImg.setId((m+1)+100);
                                                remoteImg.setContentDescription(z.getString("alumno"));
                                                ImageView im = (ImageView)findViewById(R.id.selection_);
                                                seca[0][m] = z.getString("alumno");
                                                im.setId(R.id.selection_+(m+1)+100);
                                                remoteImg.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        String[] gogo = stable.getInteropTempValue(Byte.parseByte("0"));
                                                        for (byte g=0;g<gogo.length;g++){
                                                            ImageView iman = (ImageView)findViewById(R.id.selection_+(g+1)+100);
                                                            if (v.getId()==(g+1)+100){
                                                                TextView sel = (TextView)findViewById(R.id.capturedAlumnoSelected);
                                                                sel.setText(v.getContentDescription().toString());
                                                                selectedOptions[0] = v.getContentDescription().toString();
                                                                iman.setVisibility(View.VISIBLE);
                                                            }else{
                                                                iman.setVisibility(View.GONE);
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                            stable.interactSetCompiler(seca);
                                        }catch(JSONException e){

                                        }
                                    }else{
                                        time.postDelayed(this,500);
                                    }
                                }
                            },600);
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                asunto.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        try{
                            JSONArray mamado = new JSONArray(getting_data.getString("asunto"));
                            boolean isFound = false;
                            for(int i=0;i<mamado.length();i++){
                                if (mamado.getString(i).equalsIgnoreCase(s.toString())){
                                    getting_data.put("asunto_select",i);
                                    AutoCompleteTextView tmp = (AutoCompleteTextView)findViewById(R.id.settingAsunto);
                                    tmp.setBackground(getResources().getDrawable(R.drawable.border_native));
                                    isFound = true;
                                }
                                if (!isFound){
                                    getting_data.put("asunto_select",null);
                                }
                            }
                        }catch(JSONException e){

                        }

                    }
                });
                ramo.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        try{
                            JSONArray mamado = new JSONArray(getting_data.getString("ramo"));
                            boolean isFound = false;
                            for(int i=0;i<mamado.length();i++){
                                if (mamado.getString(i).equalsIgnoreCase(s.toString())){
                                    getting_data.put("ramo_select",i);
                                    AutoCompleteTextView tmp = (AutoCompleteTextView)findViewById(R.id.settingRamo);
                                    tmp.setBackground(getResources().getDrawable(R.drawable.border_native));
                                    isFound = true;
                                }
                                if (!isFound){
                                    getting_data.put("ramo_select",null);
                                }
                            }
                        }catch(JSONException epa){

                        }
                    }
                });

                EditText nota = (EditText)findViewById(R.id.settingNota);
                nota.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String autoevaluate = null;
                        try{
                            if (Double.parseDouble(s.toString())<1.0){
                                autoevaluate = "1.0";
                            }else if(Double.parseDouble(s.toString())>7.0){
                                autoevaluate = "7.0";
                            }
                        }catch(NumberFormatException epa){

                        }
                        if (autoevaluate!=null){
                            EditText sample = (EditText)findViewById(R.id.settingNota);
                            sample.setText(autoevaluate);
                        }
                    }
                });
                onShowingMainAccessController(false);

                client.actionRetrieveListener(Byte.parseByte("1"),null,null);
                lockActivity = true;
                time.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject stable_loaded = stable.getRemoteStableData();
                        if(stable_loaded!=null){
                            time.removeCallbacks(this);
                            lockActivity = false;

                            JSONArray stable_array = null,stable_asunto = null;
                            try{
                                stable_array = new JSONArray(stable_loaded.getString("asignatura"));
                                stable_asunto = new JSONArray(stable_loaded.getString("asunto"));
                                Log.d("stable_asunto",stable_loaded.toString());
                            }catch(JSONException esp){

                            }
                            JSONObject obj_loaded;
                            String[] n = new String[stable_array.length()];
                            JSONArray tmp = new JSONArray();
                            for(int o=0;o<stable_array.length();o++){
                                try {
                                    obj_loaded = new JSONObject(stable_array.getString(o));
                                    n[o] = obj_loaded.getString("RAMO");
                                    tmp.put(n[o]);
                                }catch(JSONException e){

                                }
                            }
                            try{
                                getting_data.put("ramo",tmp);
                            }catch(JSONException e){

                            }
                            AutoCompleteTextView regen = (AutoCompleteTextView)findViewById(R.id.settingRamo);
                            ArrayAdapter<String> adaptador = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_expandable_list_item_1, n);
                            regen.setAdapter(adaptador);
                            n = new String[stable_asunto.length()];
                            tmp = new JSONArray();
                            for(int o=0;o<stable_asunto.length();o++){
                                try {
                                    obj_loaded = new JSONObject(stable_asunto.getString(o));
                                    n[o] = obj_loaded.getString("ASUNTO");
                                    tmp.put(n[o]);
                                }catch(JSONException e){

                                }
                            }
                            try{
                                getting_data.put("asunto",tmp);
                            }catch(JSONException e){

                            }
                            AutoCompleteTextView newasunto = (AutoCompleteTextView)findViewById(R.id.settingAsunto);
                            ArrayAdapter<String> anotherAdapter = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_expandable_list_item_1, n);
                            newasunto.setAdapter(anotherAdapter);
                        }else{
                            time.postDelayed(this,100);
                        }
                    }
                },100);
                ImageButton send = (ImageButton)findViewById(R.id.sendBtnEvaluate);
                send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AutoCompleteTextView ramo = (AutoCompleteTextView) findViewById(R.id.settingRamo);
                        AutoCompleteTextView asunto = (AutoCompleteTextView) findViewById(R.id.settingAsunto);
                        EditText nota = (EditText) findViewById(R.id.settingNota);
                        EditText desc = (EditText)findViewById(R.id.settingDesc);
                        Spinner grado = (Spinner) findViewById(R.id.settingGrade);
                        String[] datas = new String[8];
                        try {
                            if (selected_custom_date.getDayOfWeek() <= 5) {
                                Double notaset;
                                try {
                                    notaset = Double.parseDouble(nota.getText().toString());
                                } catch (NumberFormatException fo) {
                                    notaset = 1.0;
                                }

                                datas[0] = selectedOptions[0];
                                datas[1] = new JSONArray(getting_data.getString("ramo")).getString(getting_data.getInt("ramo_select"));
                                datas[2] = String.valueOf(notaset);
                                datas[3] = agrio.getSimpleData()[0];
                                datas[4] = String.valueOf(grado.getSelectedItemPosition());
                                String captured = asunto.getText().toString();
                                try{
                                    captured = new JSONArray(getting_data.getString("asunto")).getString(getting_data.getInt("asunto_select"));
                                }catch(JSONException e){

                                }
                                datas[5] = captured;
                                datas[6] = selected_custom_date.toString();
                                datas[7] = desc.getText().toString();

                                ConnectionClient client = new ConnectionClient(getCacheDir());
                                client.updateRetrieveListener("apiManageNewNota", datas);
                                v.setEnabled(false);
                                time.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (stable.getStableCallbackUpdate() >= 1) {
                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Main.this);
                                            alertDialogBuilder.setTitle(R.string.recived_title);
                                            alertDialogBuilder.setIcon(android.R.drawable.checkbox_on_background);
                                            alertDialogBuilder.setMessage("Su nota a "+selectedOptions[0]+" ha sido ingresada al sistema satisfactoriamente\n\nGracias por usar Nexo.");
                                            alertDialogBuilder.setPositiveButton("OK",
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface arg0, int arg1) {

                                                        }
                                                    });
                                            AlertDialog alertDialog = alertDialogBuilder.create();
                                            alertDialog.show();
                                            current_screen = 0;
                                            onPerfilMenu();
                                        } else {
                                            Toast.makeText(Main.this, R.string.error_info_notupdated, Toast.LENGTH_LONG).show();
                                            ImageButton b = (ImageButton) findViewById(R.id.sendBtnEvaluate);
                                            b.setEnabled(true);
                                        }
                                    }
                                }, 2000);
                            }else{
                                Toast.makeText(Main.this, R.string.error_incorrect_week, Toast.LENGTH_LONG).show();
                            }
                        }catch(JSONException | NullPointerException e){
                            Toast.makeText(Main.this,R.string.error_null,Toast.LENGTH_LONG).show();
                            if (datas[1]==null){
                                ramo.setBackground(getResources().getDrawable(R.drawable.border_error));
                            }
                            if (datas[5]==null){
                                asunto.setBackground(getResources().getDrawable(R.drawable.border_error));
                            }
                        }
                    }
                });
                break;
            case 2:
                stable.jsonRemoval();
                second_menu.removeAllViews();
                another.removeAllViews();
                another.addView(LayoutInflater.from(this).inflate(R.layout.person_menu, another, false));
                mimenu.setText(R.string.evaluate);
                miRightInfo.setVisibility(View.GONE);

                client.actionRetrieveListener(Byte.parseByte("2"),null,null);
                lockActivity = true;
                time.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        //CHECK REMOTE VARIABLE VALUE FROM THE SERVER NEXOAPP.CL
                        JSONObject o = stable.getRemoteStableData();
                        if(o!=null){
                            try{
                                lockActivity = false;
                                time.removeCallbacks(this);
                                JSONArray mu = new JSONArray(o.getString("alumno"));
                                JSONObject obj;
                                /*
                                TableLayout alumnoList = (TableLayout) findViewById(R.id.tableAlumnoItems);
                                TableRow tr = new TableRow(getApplication());
                                */
                                AutoCompleteTextView homulta = (AutoCompleteTextView)findViewById(R.id.getPupiloText);
                                homulta.setThreshold(1);

                                //tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                                downloadedImage = new Drawable[mu.length()];
                                String[][] moco = new String[2][mu.length()];
                                JSONArray mem = new JSONArray(o.getString("counts"));

                                for (byte bt=0;bt<mu.length();bt++){
                                    obj = new JSONObject(mu.getString(bt));

                                    LinearLayout loader = (LinearLayout)findViewById(R.id.alumnoLineal_prototype);
                                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    final View rowme = inflater.inflate(R.layout.inflater_circusava, null);
                                    loader.addView(rowme, loader.getChildCount() - 1);

                                    CircleImageView item = (CircleImageView)findViewById(R.id.viejaAva);
                                    item.setId(bt);
                                    downloadedImage[bt] = agrio.createDrawableFromURL("https://nexoapp.cl/api/?openImg="+obj.getString("image"));
                                    item.setImageDrawable(downloadedImage[bt]);
                                    item.setContentDescription(obj.getString("nombres"));

                                    moco[0][bt] = obj.getString("nombres");
                                    moco[1][bt] = obj.getString("curso");

                                    TextView countG = (TextView)findViewById(R.id.mi_contador);
                                    countG.setId(bt+10);
                                    for(byte gg=0;gg<mem.length();gg++){
                                        JSONObject fbj = new JSONObject(mem.getString(gg));
                                        if(fbj.getString("nombres").equalsIgnoreCase(moco[0][bt])){
                                            countG.setText(fbj.getString("checked"));
                                            countG.setVisibility(View.VISIBLE);
                                        }
                                    }

                                    item.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            current_screen = 3;
                                            avatarImg.setImageDrawable(downloadedImage[v.getId()]);
                                            getNombre.setText(v.getContentDescription());
                                            miCargo.setVisibility(View.GONE);
                                            micurso.setText(stable.getInteropTempValue(Byte.parseByte("1"))[v.getId()]);
                                            onShowmeYoungProfile(true);
                                            onPerfilMenu();
                                        }
                                    });
                                }
                                stable.interactSetCompiler(moco);
                                if(mu.length()==1){
                                    gettingScreenShow(Byte.parseByte(String.valueOf(0)));
                                    current_screen = 3;
                                    onPerfilMenu();
                                }else{
                                    ArrayAdapter<String> abp = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_expandable_list_item_1, moco[0]);
                                    homulta.setAdapter(abp);
                                    homulta.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            gettingScreenShow(Byte.parseByte(String.valueOf(position)));
                                            getNombre.setText(stable.getInteropTempValue(Byte.parseByte("0"))[position]);
                                            micurso.setText(stable.getInteropTempValue(Byte.parseByte("1"))[position]);
                                            micurso.setVisibility(View.VISIBLE);
                                            current_screen = 3;
                                            onPerfilMenu();
                                        }
                                    });

                                    //alumnoList.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                                    LinearLayout line = (LinearLayout)findViewById(R.id.loaderScreen);
                                    line.setVisibility(View.GONE);
                                    ConstraintLayout select = (ConstraintLayout)findViewById(R.id.mainer);
                                    select.setVisibility(View.VISIBLE);
                                }

                            }catch(JSONException e){

                            }
                        }else{
                            time.postDelayed(this,150);
                        }
                    }
                },150);
                break;
            case 3:
                stable.jsonRemoval();
                miRightInfo.setVisibility(View.GONE);
                second_menu.removeAllViews();
                mimenu.setText(R.string.evaluate);
                another.removeAllViews();
                another.addView(LayoutInflater.from(this).inflate(R.layout.resumen_nota, another, false));

                client.actionRetrieveListener(Byte.parseByte("3"),getNombre.getText().toString(),null);

                lockActivity = true;
                time.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        JSONObject obj = stable.getRemoteStableData();
                        if(obj!=null){
                            try{
                                lockActivity = false;
                                LinearLayout loader = (LinearLayout)findViewById(R.id.loading);
                                LinearLayout peso = (LinearLayout) findViewById(R.id.getRamoID);
                                TextView progreso = (TextView)findViewById(R.id.progressText);
                                progreso.setText(R.string.procesing);
                                time.removeCallbacks(this);
                                JSONArray sm = new JSONArray(obj.getString("notas"));
                                String[] activeGrades = new String[sm.length()];
                                for(byte ra=0;ra<sm.length();ra++){
                                    JSONObject om = new JSONObject(sm.getString(ra));
                                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    final View rowme = inflater.inflate(R.layout.inflater_ramo_set, null);
                                    peso.addView(rowme, peso.getChildCount() - 1);
                                    TextView tmp_ramo = (TextView)findViewById(R.id.inputRamo);
                                    tmp_ramo.setId(R.id.inputRamo+ra+10);
                                    tmp_ramo.setText(om.getString("tipo_asignatura"));
                                    ConstraintLayout senton = (ConstraintLayout)findViewById(R.id.metelo);
                                    senton.setId(R.id.metelo+ra+10);
                                    senton.setContentDescription(om.getString("tipo_asignatura"));
                                    senton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Jdata.jsonRemoval();
                                            ConnectionClient clon = new ConnectionClient(getCacheDir());
                                            clon.actionRetrieveListener(Byte.parseByte("4"),getNombre.getText().toString(),v.getContentDescription().toString());
                                            current_screen = 4;
                                            selectedOptions = new String[3];
                                            selectedOptions[0] = getNombre.getText().toString();
                                            selectedOptions[1] = v.getContentDescription().toString();
                                            mimenu.setText(v.getContentDescription().toString());
                                            onPerfilMenu();
                                        }
                                    });
                                    TextView tmp_nota = (TextView)findViewById(R.id.counter);
                                    tmp_nota.setId(R.id.counter+ra+10);

                                    tmp_nota.setText(om.getString("promedio_final"));
                                    activeGrades[ra] = om.getString("tipo_asignatura");
                                }
                                sm = new JSONArray(obj.getString("ramo"));
                                for(byte b=0;b<sm.length();b++){
                                    JSONObject o = new JSONObject(sm.getString(b));
                                    boolean created = false;
                                    for (byte bb=0;bb<activeGrades.length;bb++){
                                        if (o.getString("RAMO").equalsIgnoreCase(activeGrades[bb])){
                                            created = true;
                                        }
                                    }
                                    if (!created){
                                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        final View rowme = inflater.inflate(R.layout.inflater_ramo_set, null);
                                        peso.addView(rowme, peso.getChildCount() - 1);
                                        TextView tmp_ramo = (TextView)findViewById(R.id.inputRamo);
                                        tmp_ramo.setId(peso.getChildCount()-1);
                                        tmp_ramo.setText(o.getString("RAMO"));
                                        tmp_ramo.setTextColor(Color.LTGRAY);
                                        tmp_ramo.setContentDescription(o.getString("RAMO"));
                                    }
                                }

                                loader.setVisibility(View.GONE);
                                peso.setVisibility(View.VISIBLE);
                                Log.d("SM",sm.toString());
                            }catch(JSONException e){
                                Log.e("ERROR PUTO",e.getMessage());
                            }
                        }else{
                            time.postDelayed(this,300);
                        }
                    }
                },300);
                break;
            case 4:
                selected_custom_date = null;
                miRightInfo.setVisibility(View.GONE);
                second_menu.removeAllViews();
                second_menu.addView(LayoutInflater.from(getApplication()).inflate(R.layout.inflater_notas, another, false));
                another.removeAllViews();
                another.addView(LayoutInflater.from(this).inflate(R.layout.more_nota, another, false));
                lockActivity = true;
                generateDateModifierHandler();
                time.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject obj = stable.getRemoteStableData();
                        if(obj!=null){
                            ownerGenerateNoteGradeSample(obj);
                        }else{
                            time.postDelayed(this,300);
                        }
                    }
                },300);
                break;
            case 100:
                Jdata.jsonRemoval();
                lockActivity = true;
                selected_custom_date = null;
                another.removeAllViews();
                another.addView(LayoutInflater.from(this).inflate(R.layout.profile, another, false));
                goHome.setVisibility(View.GONE);

                client.actionRetrieveListener(Byte.valueOf("7"),null,null);
                time.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject o = stable.getRemoteStableData();
                        if (o!=null){
                            lockActivity = false;
                            //CAPTURE TEXTS
                            TextView subname = (TextView)findViewById(R.id.txSubname);
                            TextView runs = (TextView)findViewById(R.id.txRun);
                            TextView carga = (TextView)findViewById(R.id.txBoys);
                            try{
                                JSONObject o2 = new JSONObject(o.getString("profile"));
                                subname.setText("Acceso: "+o2.getString("ultimo_acceso"));
                                runs.setText("R.U.T: "+o2.getString("run"));
                                carga.setText("Carga Total: "+o.getString("carga"));
                            }catch(JSONException e){

                            }
                        }else{
                            time.postDelayed(this,1000);
                        }
                    }
                },1000);
                break;
            case 5:
                Jdata.jsonRemoval();
                selected_custom_date = null;
                another.removeAllViews();
                another.addView(LayoutInflater.from(this).inflate(R.layout.more_nota, another, false));
                addingToolbarFrame(true);
                LinearLayout an = (LinearLayout)findViewById(R.id.addnote);
                an.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!lockActivity) {
                            addingToolbarFrame(false);
                            current_screen = 1;
                            onPerfilMenu();
                        }
                    }
                });

                generateDateModifierHandler();
                client.actionRetrieveListener(Byte.valueOf("4"),agrio.getSimpleData()[0],null);
                time.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject c = stable.getRemoteStableData();
                        if (c!=null){
                            ownerGenerateNoteGradeSample(c);
                        }
                    }
                },1000);
                break;
        }
    }
    private void addingToolbarFrame (boolean inShow){
        LinearLayout toolbar_evaluator = (LinearLayout)findViewById(R.id.toolbar_evaluator_mode);
        toolbar_evaluator.setVisibility(inShow ? View.VISIBLE:View.GONE);
    }
    private void generateDateModifierHandler(){
        ImageButton prevCalSel = (ImageButton) findViewById(R.id.prevCal);
        prevCalSel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_custom_date!=null){
                    DateTimeFormatter f = DateTimeFormat.forPattern("yyyy-MM-dd");
                    boolean lessyear = false;
                    int month = selected_custom_date.getMonthOfYear()-1;
                    if (month<=0){
                        month = 12;
                        lessyear = true;
                    }
                    selected_custom_date = f.parseLocalDate((lessyear ? selected_custom_date.getYear()-1:selected_custom_date.getYear())+"-"+month+"-"+selected_custom_date.getDayOfMonth());
                    ownerGenerateNoteGradeSample(stable.getRemoteStableData());
                }
            }
        });
        ImageButton nextCalSel = (ImageButton) findViewById(R.id.nextCal);
        nextCalSel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selected_custom_date!=null){
                    DateTimeFormatter f = DateTimeFormat.forPattern("yyyy-MM-dd");
                    boolean lessyear = false;
                    int month = selected_custom_date.getMonthOfYear()+1;
                    if (month>=13){
                        month = 1;
                        lessyear = true;
                    }
                    selected_custom_date = f.parseLocalDate((lessyear ? selected_custom_date.getYear()+1:selected_custom_date.getYear())+"-"+month+"-"+selected_custom_date.getDayOfMonth());
                    ownerGenerateNoteGradeSample(stable.getRemoteStableData());
                }
            }
        });
    }
    private void ownerGenerateNoteGradeSample(JSONObject obj){
        try{
            lockActivity = false;
            ProgressBar scrload = (ProgressBar)findViewById(R.id.screenLoader);
            JSONArray derrame = new JSONArray(obj.getString("notas"));
            Log.d("LOTOS",String.valueOf(derrame.length()));
            LinearLayout tabla = (LinearLayout)findViewById(R.id.lineHead);
            tabla.removeAllViews();
            LinearLayout frame = (LinearLayout)findViewById(R.id.lines);
            DateTimeFormatter formato = DateTimeFormat.forPattern("yyyy-MM-dd");

            StaticString str = new StaticString();
            byte badNotes = 0;
            double[] promoNotes = new double[derrame.length()];
            //GENERATE LABEL
            for (int luz=0;luz<derrame.length();luz++){
                JSONObject ca = new JSONObject(derrame.getString(luz));
                LocalDate inv = formato.parseLocalDate(ca.getString("fecha_evaluada"));
                if (selected_custom_date == null){
                    selected_custom_date = formato.parseLocalDate(inv.getYear()+"-"+inv.getMonthOfYear()+"-"+inv.getDayOfMonth());
                }
                TextView yy = (TextView)findViewById(R.id.calendarAnal);
                yy.setText(String.valueOf(selected_custom_date.getYear()));
                TextView mm = (TextView)findViewById(R.id.calendarViewer);
                mm.setText(str.getMonthlyStaticString(Byte.parseByte(String.valueOf(selected_custom_date.getMonthOfYear()))));
                promoNotes[luz] = Double.parseDouble(String.valueOf(ca.getString("nota")));
                if (promoNotes[luz] < 4) {
                    badNotes++;
                }

                if (selected_custom_date.getMonthOfYear() == inv.getMonthOfYear() && selected_custom_date.getYear() == inv.getYear()) {
                    LayoutInflater info = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    if (agrio.openManageSync()){
                        final View agu = info.inflate(R.layout.general_nota,null);
                        tabla.addView(agu, tabla.getChildCount()-1);
                        TextView labelA = (TextView)findViewById(R.id.inDetails);
                        labelA.setId(R.id.inDetails+(luz+1));
                        labelA.setText("Alumno: "+ca.getString("nombres")+" - "+ca.getString("fecha_evaluada")+"\nApoderado: "+ca.getString("apoderado"));
                        TextView notaA = (TextView)findViewById(R.id.txNota);
                        notaA.setId(R.id.txNota+(luz+1));
                        notaA.setText(ca.getString("nota"));
                        TextView labelB = (TextView)findViewById(R.id.frontdetail);
                        labelB.setId(R.id.frontdetail+(luz+10000000));
                        labelB.setText(ca.getString("tipo_asignatura").toUpperCase());
                        //ADDING IMAGE SAMPLE
                        CircleImageView ava_img = (CircleImageView)findViewById(R.id.iCirclePerson);
                        ava_img.setId(R.id.iCirclePerson+(luz+1));
                        ava_img.setImageDrawable(agrio.createDrawableFromURL("https://nexoapp.cl/api/?openImg="+ca.getString("image")));
                    }else{
                        final View view_info = info.inflate(R.layout.inflater_newstyle, null);
                        tabla.addView(view_info, tabla.getChildCount() - 1);

                        TextView no = (TextView) findViewById(R.id.notav);
                        no.setId(R.id.notav + luz + 10);
                        no.setText(String.valueOf(promoNotes[luz]));

                        if (promoNotes[luz] < 4) {
                            no.setBackground(getResources().getDrawable(R.drawable.circular));
                        }

                        TextView de = (TextView) findViewById(R.id.details_txt);
                        de.setId(R.id.details_txt + luz + 10);
                        de.setText(ca.getString("razon") + " - " + ca.getString("fecha_evaluada"));

                        ImageView coquito = (ImageView) findViewById(R.id.checkedtrue);
                        coquito.setId(R.id.checkedtrue+10000+Integer.parseInt(ca.getString("ID")));
                        if (ca.getString("revisado").equalsIgnoreCase("1")) {
                            coquito.setVisibility(View.VISIBLE);
                        }
                        ConstraintLayout malo = (ConstraintLayout)findViewById(R.id.acceso);

                        malo.setId(Integer.parseInt(ca.getString("ID")));
                        String donwea = ca.getString("descripcion");
                        malo.setContentDescription(ca.getString("razon")+"\n\n" +
                                "Fecha de evaluación: "+ca.getString("fecha_evaluada")+"\n\nDescripción:\n"+(donwea.length()>0 ? donwea:"No hay descripción para esta evaluación."));
                        malo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int[] grueso = {R.string.asuntostr,android.R.drawable.checkbox_on_background};
                                StaticString.getInternalNotaDialog(Main.this,grueso,v.getContentDescription().toString()).show();
                                ConnectionClient cone = new ConnectionClient(getCacheDir());
                                String[] semen = new String[4];
                                semen[0] = agrio.getSimpleData()[0];
                                semen[1] = selectedOptions[0];
                                semen[2] = selectedOptions[1];
                                semen[3] = String.valueOf(v.getId());
                                selectedOptions[2] = semen[3];
                                cone.updateRetrieveListener("sendAnCheck", semen);
                                time.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (stable.getStableCallbackUpdate()>=1){
                                            ImageView cosita = (ImageView) findViewById(R.id.checkedtrue+10000+Integer.parseInt(selectedOptions[2]));
                                            cosita.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }, 1000);
                            }
                        });
                    }
                }
            }
            try {
                TextView badno = (TextView) findViewById(R.id.badGradeCounter);
                badno.setText(String.valueOf(badNotes));
                TextView prono = (TextView) findViewById(R.id.finalGrade);
                double lastGrade = 0.0;
                for (byte total = 0; total < promoNotes.length; total++) {
                    lastGrade += promoNotes[total];
                    if (total + 1 >= promoNotes.length) {
                        lastGrade = lastGrade / (total + 1);
                    }
                }
                prono.setText(String.valueOf(StaticString.round(lastGrade, 1)));
            }catch(NullPointerException nu){

            }
            scrload.setVisibility(View.GONE);
            frame.setVisibility(View.VISIBLE);
        }catch(JSONException e){

        }
    }
}
