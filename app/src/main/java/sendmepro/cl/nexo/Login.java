package sendmepro.cl.nexo;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import client.ClientSession;
import require.ConnectionClient;
import sendmepro.cl.nexo.ref_main.Main;
import static_tmp.AccessClient;

public class Login extends AppCompatActivity {
    private Button enterLogin,cancelRecover;
    private TextView enterRecovery,status,enterRegister;
    private EditText user_stable,pass_stable;
    private ScrollView frameSession,recovery;
    private static Boolean recoverMode,getRemoteAccess = false,internalError = false;
    private static byte timeless = 0,timeWaiting = 3;
    private ConnectionClient connect;
    private Handler h = new Handler();
    @Override
    protected void onCreate(Bundle f){
        super.onCreate(f);
        setContentView(R.layout.nexo_login);
        enterLogin = (Button)findViewById(R.id.enterAccess);
        enterRecovery = (TextView)findViewById(R.id.btnRecover);
        user_stable = (EditText)findViewById(R.id.erun);
        pass_stable = (EditText)findViewById(R.id.epsw);
        status = (TextView)findViewById(R.id.ero);
        enterLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    connect = new ConnectionClient(getCacheDir());
                    ClientSession peticion = new ClientSession();
                    peticion.setCredentialAuthMode(user_stable.getText().toString().trim(),pass_stable.getText().toString().trim(),false);
                    connect.actionRemoteLoginServer(peticion);
                    onEnterFrameLogin(false);
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            timeless++;
                            if (!internalError){
                                if(!getRemoteAccess && timeless < timeWaiting){
                                    h.postDelayed(this,1000);
                                }else{
                                    onEnterFrameLogin(true);
                                    h.removeCallbacks(this);
                                    if (getRemoteAccess){
                                        appendAccess(true);
                                    }else{
                                        Toast.makeText(getApplication(), "Nombre de usuario o contraseña incorrectos, por favor intentelo nuevamente.", Toast.LENGTH_SHORT).show();
                                        if (timeWaiting < 9){
                                            timeWaiting ++;
                                        }
                                    }
                                }
                            }
                            else{
                                onEnterFrameLogin(true);
                                h.removeCallbacks(this);
                                Toast.makeText(getApplication(), "No hay conexión a internet o el servicio de autenticación no esta disponible",Toast.LENGTH_LONG).show();
                            }
                        }
                    },1000);
            }
        });
        enterRegister = (TextView)findViewById(R.id.hregister);
        enterRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appendAccess(false);
            }
        });
        enterRecovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterRecoveryMode(true);
            }
        });
        cancelRecover = (Button)findViewById(R.id.btnBack);
        cancelRecover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterRecoveryMode(false);
            }
        });
        frameSession = (ScrollView) findViewById(R.id.loginScreen);
        recovery = (ScrollView) findViewById(R.id.recoverScreen);
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT_WATCH) {
            boxMaximunCompatibilityColorMode();
        }
        pass_stable.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boxChecker();
            }
        });
        user_stable.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boxChecker();
            }
        });
    }

    private void onEnterFrameLogin(boolean opt){
        enterLogin.setEnabled(opt ? true:false);
        user_stable.setEnabled(opt ? true:false);
        pass_stable.setEnabled(opt ? true:false);
        internalError = false;
        timeless = 0;
    }

    public static void responseRequest(JSONObject obj,boolean error){
        if (!error) {
            try {
                getRemoteAccess = obj.getBoolean("status");
                JSONObject ojete = new JSONObject(obj.getString("nexo"));
                AccessClient.rewriteTmpAccess(ojete);
            } catch (JSONException ep) {
                getRemoteAccess = false;
            }
        }else{
            internalError = true;
        }
    }
    private void boxChecker(){
        if(user_stable.getText().length()>4 && pass_stable.getText().length()>4){
            enterLogin.setEnabled(true);
        }else{
            enterLogin.setEnabled(false);
        }
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT_WATCH){
            boxMaximunCompatibilityColorMode();
        }
    }
    private void appendAccess(boolean access){
        getRemoteAccess = false;
        timeWaiting = 3;
        user_stable.setText(null);
        pass_stable.setText(null);
        Intent i = new Intent(Login.this,access?Main.class:Register.class);
        startActivity(i);
    }
    private void boxMaximunCompatibilityColorMode(){
        enterLogin.setBackgroundColor(Color.argb(enterLogin.isEnabled()?255:70, 175, 123, 224));
    }
    private void enterRecoveryMode(Boolean recover){
        frameSession.setVisibility(recover?View.GONE:View.VISIBLE);
        recovery.setVisibility(recover?View.VISIBLE:View.GONE);
        status.setText(recover?"Recuperar Acceso":"¡Bienvenido!");
    }
}
