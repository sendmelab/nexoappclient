package sendmepro.cl.nexo;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import require.ConnectionClient;

public class Register extends AppCompatActivity {
    private String[] selectedType={"Apoderado","Profesor","Director"};
    private byte selectedItem = 0;
    private static byte resendStatement = 0;
    private Button mailer;
    private RadioButton gopt1,gopt2,gopt3;
    private EditText mailerSign;
    private ProgressBar loadScreen;
    private ConstraintLayout adbAnimationScreen;
    private ImageView loadWarningScreen;
    private TextView loadInputTextScreen;
    @Override
    protected void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.nexo_register);
        /*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, avalueType);
        gradeType.setAdapter(adapter);
        */
        mailer = (Button)findViewById(R.id.btRegisterMe);
        mailerSign = (EditText)findViewById(R.id.emailSender);
        gopt1 = (RadioButton)findViewById(R.id.rar_option1);
        gopt2 = (RadioButton)findViewById(R.id.rar_option2);
        gopt3 = (RadioButton)findViewById(R.id.rar_option3);

        adbAnimationScreen = (ConstraintLayout)findViewById(R.id.adbScreenAnimatedFocus);
        loadScreen = (ProgressBar)findViewById(R.id.loaderScreen);
        loadWarningScreen = (ImageView)findViewById(R.id.imgWarning);
        loadInputTextScreen = (TextView)findViewById(R.id.viejoPuto);

        gopt1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    selectedItem = 0;
                }
            }
        });
        gopt2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    selectedItem = 1;
                }
            }
        });
        gopt3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    selectedItem = 2;
                }
            }
        });
        mailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoRegisteringPerson();
            }
        });

        mailerSign.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkValidate();
            }
        });
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT_WATCH){
            settingCompatibilityColor(false);
        }
        mailerSign.setText("@domain");
    }
    public static void valueChangeSelect(Boolean state){
        resendStatement = state ? Byte.parseByte("1"):Byte.parseByte("2");
    }
    private void gotoRegisteringPerson(){
        try {
            //Map<String, JSONObject> parameter = new HashMap();
            JSONObject object = new JSONObject();
            object.put("personal_email",mailerSign.getText().toString().trim());
            object.put("personal_charge",selectedType[selectedItem]);
            //parameter.put("nexoSenderClient",object);

            ConnectionClient regen = new ConnectionClient(getCacheDir());
            regen.actionRegisterConfirmation(object,null);
            //regen.actionRegisterCustomConfirmation(parameter);
        }catch(JSONException ex){

        }
    }
    private void checkValidate(){
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = mailerSign.getText().toString().trim();
        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        mailer.setEnabled(matcher.matches());

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT_WATCH){
            settingCompatibilityColor(matcher.matches());
        }
    }
    private void settingCompatibilityColor(Boolean as) {
        mailer.setBackgroundColor(Color.argb(as ? 255 : 90, 175, 123, 224));
    }
}
