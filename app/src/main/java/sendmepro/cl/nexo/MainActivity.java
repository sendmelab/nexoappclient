package sendmepro.cl.nexo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import slide.Sample;

public class MainActivity extends AppCompatActivity {
    private Button t;
    private ViewPager mom;
    private Sample momLoad;
    private Byte item_selected = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int[] draw = {R.drawable._x0,R.drawable._x1,R.drawable._x2};
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        t = (Button)findViewById(R.id.btnLogin);
        t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent com = new Intent(MainActivity.this,Login.class);
                startActivity(com);
                finish();
            }
        });
        mom = (ViewPager)findViewById(R.id.slider_yonito);
        momLoad = new Sample(this);
        momLoad.setNewDrawableSamples(draw);
        mom.setAdapter(momLoad);
        final Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mom.getAdapter().getCount() <= item_selected){
                    item_selected=0;
                }else{
                    item_selected++;
                }
                mom.setCurrentItem(item_selected);
                h.postDelayed(this,8000);
            }
        },8000);
        mom.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                item_selected = Byte.parseByte(String.valueOf(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
